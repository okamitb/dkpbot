import os
import discord
import random
import shutil
import json
from discord.ext import commands
from datetime import datetime

file = open("secrets\\keys.txt")
TOKEN = file.readline().rstrip()
GUILD = file.readline().rstrip()

secrets_dir = "secrets\\"
reports_dir = "reports\\"
dkp_file_name = "dkp-current.csv"
raid_file_name = "raids.json"
dkp_file = reports_dir + dkp_file_name
raid_file = reports_dir + raid_file_name
DKP = {}
RAIDS = {}
ROLES = ['Officer', 'Titan', 'Legend', 'Star']

def load_dkp():
    if os.path.exists(dkp_file):
        file = open(dkp_file, 'r')
        for line in file:
            data = line.split(',')
            player = data[0]
            points = int(data[1])
            DKP[player] = points
        file.close()

def save_file():
    file = open(dkp_file, 'w')
    points = [(k, DKP[k]) for k in sorted(DKP, key=DKP.get, reverse=True)]
    for raider in points:
        file.write(f'{raider[0]},{raider[1]}\n')
    file.close()
    
def load_raids():
    if os.path.exists(raid_file):
        with open(raid_file) as json_file:
            data = json.load(json_file)
            for raid in data:
                RAIDS[raid] = data[raid]

def save_raids():
    with open(raid_file, 'w') as outfile:
        json.dump(RAIDS, outfile)

##########################################
bot = commands.Bot(command_prefix='$')
##########################################

@bot.event
async def on_ready():
    load_dkp()
    load_raids()
    backup_file = 'reports\dkp-backup.csv'
    shutil.copyfile(dkp_file, backup_file)
    guild = discord.utils.get(bot.guilds, id=GUILD)
    for guild in bot.guilds:
        print(f'{bot.user} has connected to {guild.name}: {guild.id}!')
		

@bot.command(name='addmember')
@commands.has_any_role('Officer','Guild Master')
async def add_guild_member(ctx, name, starting=0):
    if not name in DKP:
        DKP[name] = starting
        response = f'{name} has been added to the guild roster with {starting} DKP.'
    else:
        response = f'{name} has already been added to the guild roster.'
    
    save_file()    
    await ctx.send(response)


@bot.command(name='removegmember')
@commands.has_any_role('Officer','Guild Master')
async def remove_guild_member(ctx, name):
    if name in DKP:
        DKP.pop(name)
        response = f'{name} has been removed from the guild roster.'
    else:
        response = f'{name} is not a registered guild member.'

    await ctx.send(response)

@bot.command(name='addmembers')
@commands.has_any_role('Officer','Guild Master')
async def add_guild_members(ctx, *names):
    if len(names) > 0:
        for name in names:
            if not name in DKP:
                DKP[name] = 0
                response = f'{name} has been added to the guild roster.'
            else:
                response = f'{name} has already been added to the guild roster.'
            await ctx.send(response)
    else:
        response = "Please supply names to add to the guild."
        await ctx.send(response)
    
    save_file() 
     

@bot.command(name='guildlist')
async def list_guild_members(ctx):
    if len(DKP) > 0:
        response = "Full Clear Guild Members:\n"
        for name in [*DKP]:
            response += f"\t{name}\n"
    else:
        response = "No guild members have been registered."
        
    await ctx.send(response)


@bot.command(name='add')
@commands.has_any_role('Officer','Guild Master')
async def add_dkp(ctx, name, amount):
    new_dkp = int(amount)
    if name in DKP:
        DKP[name] += new_dkp
    else:
        DKP[name] = new_dkp
    
    response = f'{name} has received {amount} DKP and now has {DKP[name]} DKP total.'
    save_file()
    await ctx.send(response)


@bot.command(name='addguild')
@commands.has_any_role('Officer','Guild Master')
async def add_dkp_guild(ctx, amount):
    new_dkp = int(amount)
    for player in DKP:
        DKP[player] += new_dkp
    
    response = f'{new_dkp} DKP now has been given to all guild members.'
    save_file()
    await ctx.send(response)
    

@bot.command(name='addraid')
@commands.has_any_role('Officer','Guild Master')
async def add_dkp_raid(ctx, raid_name, amount):
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        new_dkp = int(amount)
        for player in raiders:
            DKP[player] += new_dkp
            raiders[player] += new_dkp
        
        response = f'{new_dkp} DKP now has been given to all members raiding {raid_name}.'
    else:
        response = f'{raid_name} is not an active raid.'
    
    save_raids()
    save_file()
    await ctx.send(response)


@bot.command(name='addraider')
@commands.has_any_role('Officer','Guild Master')
async def add_dkp_to_raider(ctx, raider, raid_name, amount):
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        if raider in raiders:
            raiders[raider] += int(amount)
            DKP[raider] -+ int(amount)
            response = f'{raider} has received {amount} DKP in {raid_name}.'
        else:
            response = f'{raider} is not registered for {raid_name}.'
    else:
        response = f'{raid_name} is not an active raid.'

    save_raids()
    await ctx.send(response)


@bot.command(name='subraider')
@commands.has_any_role('Officer','Guild Master')
async def sub_dkp_from_raider(ctx, raider, raid_name, amount):
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        if raider in raiders:
            raiders[raider] -= int(amount)
            DKP[raider] -= int(amount)
            response = f'{amount} DKP has been deducted from {raider} in {raid_name}.'
        else:
            response = f'{raider} is not registered for {raid_name}.'
    else:
        response = f'{raid_name} is not an active raid.'
    save_raids()
    await ctx.send(response)

@bot.command(name='sub')
@commands.has_any_role('Officer','Guild Master')
async def subtract_dkp(ctx, name, amount):
    new_dkp = int(amount)
    if name in DKP:
        DKP[name] -= new_dkp
    else:
        DKP[name] = 0 - new_dkp
    
    response = f'{name} now has {DKP[name]} DKP.'
    save_file()
    await ctx.send(response)


@bot.command(name='subguild')
@commands.has_any_role('Officer','Guild Master')
async def subtract_dkp_guild(ctx, amount):
    new_dkp = int(amount)
    for player in DKP:
        DKP[player] -= new_dkp
    
    response = f'{new_dkp} DKP now has been given to all guild members.'
    save_file()
    await ctx.send(response)
    
 
@bot.command(name='subraid')
@commands.has_any_role('Officer','Guild Master')
async def add_dkp_raid(ctx, raid_name, amount):
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        new_dkp = int(amount)
        for player in raiders:
            DKP[player] -= new_dkp
            raiders[player] -= new_dkp
        response = f'{new_dkp} DKP now has been deducted from all members raiding {raid_name}.'
    else:
        response = f'{raid_name} is not an active raid.'
    
    save_file()
    save_raids()
    await ctx.send(response)

 
@bot.command(name='dkp')
async def player_dkp(ctx, name=None):
    if not name:
        if ctx.author.name in DKP:
            name = ctx.author.name
            response = f'{name} has {DKP[name]} DKP.'
        elif ctx.author.nick in DKP:
            name = ctx.author.nick
            response = f'{name} has {DKP[name]} DKP.'
    elif name in DKP:
        response = f'{name} has {DKP[name]} DKP.'
    else:
        response = f'{name} has no recorded DKP.'
        
    await ctx.send(response)
    

@bot.command(name='dkpguild')
async def guild_dkp(ctx):
    
    if len(DKP) > 0:
        response = "Guild DKP Standings:\n"
        points = [(k, DKP[k]) for k in sorted(DKP, key=DKP.get, reverse=True)]
        
        for k, v in points:
            response += f'\t\t{k}: {v}\n'
    else:
        response = "No guild members have been registered."
        
    await ctx.send(response)
        

@bot.command(name='startraid')
@commands.has_any_role('Officer','Guild Master')
async def start_raid(ctx, raid_name):
    if raid_name in RAIDS:
        response = f'Raid "{raid_name}" already exists.'
    else:
        RAIDS[raid_name] = [{}, False]
        response = f'Raid "{raid_name}" has been added (registration closed).'
    save_raids()    
    await ctx.send(response)
        

@bot.command(name='stopraid')
@commands.has_any_role('Officer','Guild Master')
async def stop_raid(ctx, raid_name):
    if raid_name in RAIDS:
        RAIDS.pop(raid_name)
        response = f'{raid_name} has been deleted from active raids.'
    else:
        response = f'{raid_name} is not the name of an active raid.'
    save_raids()    
    await ctx.send(response)


@bot.command(name='showraids')
async def show_raids(ctx):
    if len(RAIDS) > 0:
        response = "These raids are currently active:\n"
        for raid in RAIDS:
            reg = "Open" if RAIDS[raid][1] else "Closed"
            response += f'\t{raid}: Registration is {reg}.\n'
    else:
        response = "There are no registered raids."
    
    await ctx.send(response)


@bot.command(name='openraid')
@commands.has_any_role('Officer','Guild Master')
async def open_raid(ctx, raid_name):
    if raid_name in RAIDS:
        RAIDS[raid_name][1] = True
        response = f'{raid_name} has been opened for registration.'
    else:
        response = f'{raid_name} is not the name of an active raid.'
        save_raids()
    await ctx.send(response)


@bot.command(name='closeraid')
@commands.has_any_role('Officer','Guild Master')
async def close_raid(ctx, raid_name):
    if raid_name in RAIDS:
        RAIDS[raid_name][1] = False
        response = f'{raid_name} registration is now closed.'
    else:
        response = f'{raid_name} is not the name of an active raid.'
    save_raids()
    await ctx.send(response)


@bot.command(name='regraider')
@commands.has_any_role('Officer','Guild Master')
async def register_raider(ctx, name, raid_name):
    if name in DKP:
        if raid_name in RAIDS:
            raid = RAIDS[raid_name]
            raiders = raid[0]
            if not name in raiders:
                raiders[name] = 0
                response = f'{name} has been registered for {raid_name}.'
            else:
                response = f'{name} is already registered for {raid_name}.'
        else:
            response = f'{raid_name} is not an active raid.'
    else:
        response = f'Cannot register {name} - they have not been registered in the guild roster.'
    save_raids()
    await ctx.send(response)
    

@bot.command(name='regraiders')
@commands.has_any_role('Officer','Guild Master')
async def add_guild_members(ctx, raid_name, *names):
    
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        for name in names:
            if name in DKP:
                if not name in raiders:
                    raiders[name] = 0
                    response = f'{name} has been registered for {raid_name}.'
                    await ctx.send(response) 
                else:
                    response = f'{name} is already registered for {raid_name}.'
                    await ctx.send(response) 
            else:
                response = f'Cannot register {name} - they have not been registered in the guild roster.'
                await ctx.send(response) 
    else:
        response = f'{raid_name} is not an active raid.'
        await ctx.send(response) 
    
    save_raids()


@bot.command(name='register')
async def register_self(ctx, raid_name):
    name = False
    if ctx.author.name in DKP:
        name = ctx.author.name
    elif ctx.author.nick in DKP:
        name = ctx.author.nick
    if name:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        open = raid[1]
        if open:
            if not name in raiders:
                raiders[name] = 0
                response = f'{name} has been registered for {raid_name}.'
            else:
                repsonse = f'{name} is already registered for {raid_name}.'
        else:
            response = f'{raid_name} is not open for registration.'
    else:
        response = f'{ctx.author.name} cannot be registered for {raid_name} until they are registered in the guild roster.'
    save_raids()
    await ctx.send(response)
    
    
@bot.command(name='removeraider')
@commands.has_any_role('Officer','Guild Master')
async def remove_raider(ctx, name, raid_name):
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        if name in raiders:
            raiders.remove(name)
            response = f'{name} has been removed from {raid_name} registration.'
        else:
            response = f'{name} has not been registered for {raid_name}.'
    else:
        response = f'{raid_name} is not an active raid.'
    save_raids()
    await ctx.send(response)


@bot.command(name='raidlist')
async def list_raid_members(ctx, raid_name):
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        if len(raiders) > 0:
            response = f'Raiders registered for MC:\n'
            for name in raiders:
                response += f'\t{name}\n'
        else:
            response = f'There are no members registered for {raid_name}'    
    else:
        response = f'{raid_name} is not an active raid.'

    await ctx.send(response)


@bot.command(name='dkpraid')
async def raid_dkp(ctx, raid_name):
    if raid_name in RAIDS:
        raid = RAIDS[raid_name]
        raiders = raid[0]
        if len(raiders) > 0:
            response = f'DKP Standings for {raid_name}:\n'
            pointdict = {}
            for raider in raiders:
                pointdict[raider] = DKP[raider]
            points = [(k, pointdict[k]) for k in sorted(pointdict, key=pointdict.get, reverse=True)]
            for k, v in points:
                response += f'\t\t{k}: {v}\n'
        else:
            response = f'No one has been registered for {raid_name}.'
    else:
        response = f'{raid_name} is not an active raid.'
    
    await ctx.send(response)


@bot.command(name='export')
@commands.has_any_role('Officer','Guild Master')
async def export_dkp_csv(ctx):
    area=ctx.message.channel
    now = str(datetime.now().date())
    export_file_name = f'DKP-guild-{now}.csv'
    export_file = reports_dir + export_file_name
    shutil.copyfile(dkp_file, export_file)
    with open(export_file, 'rb') as csv:
        await area.send(file=discord.File(csv, export_file_name))


@bot.command(name='dkpearned')
async def show_earned_dkp(ctx, raid_name):
    if raid_name in RAIDS:
        raiders = RAIDS[raid_name][0]
        response = f'DKP earned in {raid_name}:\n'
        for name in raiders:
            response += f'\t{name}: {raiders[name]}\n'
    else:
        response = f'{raid_name} is not an active raid.'

    await ctx.send(response)


@bot.command(name='saveraid')
@commands.has_any_role('Officer','Guild Master')
async def save_raid_dkp(ctx, raid_name):
    if raid_name in RAIDS:
        area=ctx.message.channel
        
        raid = RAIDS[raid_name]
        raiders = raid[0]

        now = str(datetime.now().date())
        export_file_name = f'DKP-MC-{now}.csv'
        export_file = reports_dir + export_file_name
        file = open(export_file, 'w')
        for name in raiders:
            file.write(f'{name},{raiders[name]}\n')
        file.close()
        with open(export_file, 'rb') as csv:
            await area.send(file=discord.File(csv, export_file_name))

    else:
        response = f'{raid_name} is not an active raid.'
        await ctx.send(response)


@bot.command(name="saveraidfile")
@commands.has_any_role('Officer','Guild Master')
async def save_raid_file(ctx):
    save_raids()
    response = "Raid file has been updated."
    await ctx.send(response)


@bot.command(name="detectroster")
@commands.has_any_role('Officer','Guild Master')
async def add_bulk_guildmembers(ctx):
    guild = bot.guilds[0]
    count = 0
    added = []
    for role in guild.roles:
        if role.name in ROLES:
            for member in role.members:
                if member.name not in DKP:
                    DKP[member.name] = 0
                    added.append(member.name)
                    count += 1
    response = f'{count} members have been added to the guild:\n{added}'
    await ctx.send(response)
    

@bot.command(name="shutdown")
@commands.has_any_role('Officer','Guild Master')
async def shut_down(ctx):
    await bot.logout()


bot.run(TOKEN)